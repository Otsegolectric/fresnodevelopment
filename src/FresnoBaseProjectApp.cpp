
#include "FresnoBaseProjectApp.h"


FresnoBaseProjectApp::FresnoBaseProjectApp(void) {
	effectID = 51;
}

FresnoBaseProjectApp::~FresnoBaseProjectApp(void) { }

void FresnoBaseProjectApp::InitDevMode() {
	myTexture = loadImage(loadAsset( "dot.png" ));

	//TODO
	setCanvasSizeVars(getWindowSize().x, getWindowSize().y);
}

void FresnoBaseProjectApp::setCanvasSizeVars(float w, float h) {
	canvasWidth = w;
	canvasHeight = h;
	canvasHalfWidth = canvasWidth * 0.5f;
	canvasHalfHeight = canvasHeight * 0.5f;
}

//should only occur in dev (standalone) mode
void FresnoBaseProjectApp::setup() {
#ifdef DEV_MODE
	InitDevMode();
	globalSetup();
#endif

}

void FresnoBaseProjectApp::mouseDown( MouseEvent event ) {
}

void FresnoBaseProjectApp::update() {
	Update();
}

void FresnoBaseProjectApp::draw() {
	Draw();
}

void FresnoBaseProjectApp::Update() {

	isLocked = true;

	counter++;
	redVal = sin(counter * 0.01f);
	redVal = lmap(redVal, -1.0f, 1.0f, 0.0f, 1.0f);
	blueVal = sin(counter * 0.013f);
	redVal = lmap(blueVal, -1.0f, 1.0f, 0.0f, 1.0f);
	greenVal = sin(counter * 0.017f);
	redVal = lmap(greenVal, -1.0f, 1.0f, 0.0f, 1.0f);

	pe.update();

	isLocked = false;

}
void FresnoBaseProjectApp::Draw() {

	isLocked = true;

#ifdef DEV_MODE
	gl::pushMatrices();
	gl::translate(canvasHalfWidth, canvasHalfHeight); 
#endif

	//gl::clear( Color( redVal, blueVal, greenVal ) ); 
	gl::clear( Color( 0, 0, 0 ) ); 

	gl::enableAlphaBlending();
	//gl::draw( myTexture, Rectf(-50, -50, 50, 50) );
	pe.render();
	gl::disableAlphaBlending();

#ifdef DEV_MODE
	gl::popMatrices();
#endif

	isLocked = false;

}

void FresnoBaseProjectApp::globalSetup() {

	pe.setup(myTexture);
	counter = 0;
	int numParticles = 10;
	for (int i = 0; i < numParticles; i++) {
		Particle p(canvasWidth, canvasHeight);
		pe.add(p);
	}

}

void FresnoBaseProjectApp::Initialize(char* InitData, Timeline* CinderTimeline) {

	int length=0;
	while (InitData[length++] != '}'){}

	char* JSON = new char[length];
	for (int i = 0; i < length; i++){
		JSON[i] = InitData[i];
	}
	JSON[length] = '\0';

	rapidjson::Document data;
	data.Parse<0>(JSON);

	canvasWidth = (float)data["canvasWidth"].GetDouble();
	canvasHeight = (float)data["canvasHeight"].GetDouble();
	myTexturePath = (string)data["texturePath"].GetString();

	std::stringstream ss;
	std::string s;
	ss << std::noskipws << myTexturePath;
	s = ss.str();

	if(!s.empty()){
		myTexture = gl::Texture(loadImage(s.c_str()));	
	} else {
		myTexture = gl::Texture(loadImage("DefaultAssets/00051/dot.png"));
	}

	setCanvasSizeVars(canvasWidth, canvasHeight);
	globalSetup();

}

void FresnoBaseProjectApp::SetEffectData(char* Data) {

}

Vec2f getWorldCoords(Vec2f normalizedCoords) {
	Vec2f worldCoords;
#ifdef DEV_MODE

#else
#endif

	return worldCoords;
}

#ifdef DEV_MODE
CINDER_APP_NATIVE( FresnoBaseProjectApp, RendererGl )
#else
extern "C"{
	__declspec(dllexport) IEffectInterface* GetInterface(){
		IEffectInterface * effect = new FresnoBaseProjectApp();
		return effect;
		}}
#endif