//Fresno particle class
//particle speeds are normalized then scaled by the width of the canvas

#pragma once

#include "cinder/Vector.h"
#include <vector>
#include "cinder/gl/gl.h"
#include "cinder/Rand.h"
#include "cinder/app/AppBasic.h"
#include "cinder/Color.h"

using namespace ci;
using namespace ci::app;

class Particle {
public:
	Particle(float canvasWidth, float canvasHeight);
	void update();
	Vec2f getPosition();
	float getSize();
	ColorA getColor();
private:
	Vec2f position;
	Vec2f normDirection;
	float speed;
	void reset();
	bool isDead;
	float size;
	ColorA col;
	float canvasWidth, canvasHeight, canvasHalfWidth, canvasHalfHeight;
};
