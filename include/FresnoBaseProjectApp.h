#pragma once

//#define DEV_MODE

#ifdef DEV_MODE
#include "cinder/app/AppNative.h"
#endif

#include "IEffectInterface.h"

#include "cinder/gl/gl.h"
#include "cinder/ImageIo.h"
#include "cinder/gl/Texture.h"
#include "ParticleEmitter.h"

#include "rapidjson.h"
#include "document.h"

using namespace ci;
using namespace ci::app;
using namespace std;

#ifdef DEV_MODE
class FresnoBaseProjectApp : public AppNative, public IEffectInterface	 {
#else
class FresnoBaseProjectApp : public IEffectInterface	 {
#endif

  public:

	FresnoBaseProjectApp(void);
	~FresnoBaseProjectApp(void);
	void setup();
	void mouseDown( MouseEvent event );	
	void update();
	void draw();

	void Update();
	void Draw();

	void Initialize(char* InitData, Timeline* CinderTimeline);
	void SetEffectData(char* Data);
	void InitDevMode();

private:

	int counter;
	float redVal, blueVal, greenVal;

	gl::Texture myTexture;
	string myTexturePath;
	float canvasWidth, canvasHeight;
	float canvasHalfWidth, canvasHalfHeight;

	//de-normalize coordinates
	Vec2f getWorldCoords();
	void setCanvasSizeVars(float w, float h);
	ParticleEmitter pe;
	void globalSetup(); //setup common to both dev and production DLL modes

};